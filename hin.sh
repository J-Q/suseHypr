#!/bin/bash

### Starting
# Software from repos and repos installation
sudo zypper ar -cfp 90 https://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Tumbleweed/ packman
sudo zypper dup --from packman --allow-vendor-change
sudo zypper up
sudo zypper in git exa neovim fish flatpak ranger opi xdg-user-dirs xdg-user-dirs-gtk hyprland xwayland foot neofetch gtk-layer-shell-devel swww gcc
opi codecs
# Additional user groups
sudo usermod -G audio,video,input,wheel,flatpak $USER
# Creating of base dirs
xdg-user-dirs-update --force
xdg-user-dirs-gtk-update --force
mkdir ~/.icons ~/.themes
# Flatpak misc
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak remote-add --if-not-exists kdeapps https://distribute.kde.org/kdeapps.flatpakrepo
flatpak --user override --filesystem=/home/$USER/.icons/:ro
flatpak --user override --filesystem=/home/$USER/.themes/:ro
flatpak --user override --filesystem=/usr/share/icons/:ro
flatpak --user override --filesystem=/usr/share/themes/:ro
# FiraCode Nerd font installation
cd ~/Downloads
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.0/FiraCode.zip
mkdir FiraCode
unzip FiraCode.zip -d FiraCode
rm FiraCode/LICENSE FiraCode/readme.md
sudo cp -r FiraCode /usr/share/fonts
fc-cache -v

### Minimal configuration
# Fixing font in foot
mkdir ~/.config/foot
cd ~/.config/foot
wget https://codeberg.org/dnkl/foot/raw/branch/master/foot.ini
sed -i 's/^#\s*font=\(.*size=[[:digit:]]\)/font=FiraCode Nerd Font:size=8/' foot.ini
# Making foot the default terminal emulator
mkdir ~/.config/hypr
cp /usr/share/hyprland/hyprland.conf ~/.config/hypr/
sed -i 's/kitty/foot/g' ~/.config/hypr/hyprland.conf
sed -i 's/exec, dolphin/exec, flatpak run org.kde.dolphin/g' ~/.config/hypr/hyprland.conf
#Changing the shell
chsh -s $(which fish)

### Installing EWW (future widgets)
#Installing rust (packages from repos are broken, rust sucks)
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
source "$HOME/.cargo/env"
fish -c "fish_add_path $HOME/.cargo/bin"
# Compiling EWW
cd ~/Downloads
git clone https://github.com/elkowar/eww
cd eww
cargo build --release --no-default-features --features=wayland
cp ~/Downloads/eww/target/release/eww /usr/local/bin/
rm -rf ~/Downloads/eww

### Installing base gui software
flatpak install org.kde.dolphin org.kde.gwenview org.mozilla.firefox com.github.tchx84.Flatseal org.kde.discover org.freedesktop.appstream-glib

pkill -KILL -u $USER
